package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

    public static Connection getConnection() {
//        System.out.println("Conectando ao banco!");
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            return DriverManager.getConnection("jdbc:mysql://13.254.220.126:3306/prog4all", "neppo", "123456");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        ConnectionFactory.getConnection();
        System.out.println("CONEXÃO OK!");
    }
}