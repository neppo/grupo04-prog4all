package model;

public class Exercicio {
    private int id;
    private int idLicoes;
    private int idPersona;
    private String pergunta;
    private String alternativaA;
    private String alternativaB;
    private String alternativaC;
    private String alternativaD;
    private String dica;
    private int pontuacao;

    public Exercicio(int id, int idLicoes, String pergunta, String alternativaA, String alternativaB, String alternativaC, String alternativaD, String dica, int pontuacao) {
        this.id = id;
        this.idLicoes = idLicoes;
        this.pergunta = pergunta;
        this.alternativaA = alternativaA;
        this.alternativaB = alternativaB;
        this.alternativaC = alternativaC;
        this.alternativaD = alternativaD;
        this.dica = dica;
        this.pontuacao = pontuacao;
    }

    public int getId() {
        return id;
    }

    public int getIdLicoes() {
        return idLicoes;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public String getPergunta() {
        return pergunta;
    }

    public String getAlternativaA() {
        return alternativaA;
    }

    public String getAlternativaB() {
        return alternativaB;
    }

    public String getAlternativaC() {
        return alternativaC;
    }

    public String getAlternativaD() {
        return alternativaD;
    }

    public String getDica() {
        return dica;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }
}
