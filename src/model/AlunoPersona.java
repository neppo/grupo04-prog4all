package model;

public class AlunoPersona {
    private int idPersona;
    private int idAluno;

    public AlunoPersona(int idPersona, int idAluno) {
        this.idPersona = idPersona;
        this.idAluno = idAluno;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public int getIdAluno() {
        return idAluno;
    }
}
