package model;

public class Respostas {
    private int id;
    private int idExercicio;
    private char alternativaCorreta;

    public Respostas(int id, int idExercicio, char alternativaCorreta) {
        this.id = id;
        this.idExercicio = idExercicio;
        this.alternativaCorreta = alternativaCorreta;
    }

    public int getIdExercicio() {
        return idExercicio;
    }

    public char getAlternativaCorreta() {
        return alternativaCorreta;
    }
}
