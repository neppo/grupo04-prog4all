package model;

public class Modulo {
    private int id;
    private String titulo;
    private int nivel;
    private int quantidadeLicoes;

    public Modulo(int id, String titulo, int nivel, int quantidadeLicoes) {
        this.id = id;
        this.titulo = titulo;
        this.nivel = nivel;
        this.quantidadeLicoes = quantidadeLicoes;
    }

    public int getId() {
        return id;
    }

    public String getTitulo() {
        return titulo;
    }

    public int getNivel() {
        return nivel;
    }

    public int getQuantidadeLicoes() {
        return quantidadeLicoes;
    }


}
