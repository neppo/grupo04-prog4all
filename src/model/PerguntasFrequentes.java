package model;

public class PerguntasFrequentes {
    private String pergunta;
    private String resposta;

    public PerguntasFrequentes(String pergunta, String resposta) {
        this.pergunta = pergunta;
        this.resposta = resposta;
    }

    public String getPergunta() {
        return pergunta;
    }

    public String getResposta() {
        return resposta;
    }
}
