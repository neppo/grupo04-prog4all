package model;

public class Licoes {
    private int id;
    private int idPersona;
    private int idModulo;
    private String descricao;
    private int quantidadeExercicios;

    public Licoes(int id, int idModulo, String descricao, int quantidadeExercicios) {
        this.id = id;
        this.idModulo = idModulo;
        this.descricao = descricao;
        this.quantidadeExercicios = quantidadeExercicios;
    }

    public int getId() {
        return id;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public int getIdModulo() {
        return idModulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public int getQuantidadeExercicios() {
        return quantidadeExercicios;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

}
