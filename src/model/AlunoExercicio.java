package model;

public class AlunoExercicio {
    private int idAluno;
    private int idExercicio;

    public AlunoExercicio(int idAluno, int idExercicio) {
        this.idAluno = idAluno;
        this.idExercicio = idExercicio;
    }

    public int getIdAluno() {
        return idAluno;
    }

    public int getIdExercicio() {
        return idExercicio;
    }
}
