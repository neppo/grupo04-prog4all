package model;

public class Aluno {
    private int id;
    private int idUsuario;
    private String dtNascimento;
    private char genero;
    private String apelido;
    private String instituicao;
    private boolean conheceProgramacao;
    private int nivel;
    private int saldoPontuacao;

    public Aluno(String dtNascimento, char genero, boolean conheceProgramacao) {
        this.dtNascimento = dtNascimento;
        this.genero = genero;
        this.conheceProgramacao = conheceProgramacao;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public void setInstituicao(String instituicao) {
        this.instituicao = instituicao;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public void setSaldoPontuacao(int saldoPontuacao) {
        this.saldoPontuacao = saldoPontuacao;
    }

    public int getId() {
        return id;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public String getDtNascimento() {
        return dtNascimento;
    }

    public String getApelido() {
        return apelido;
    }

    public String getInstituicao() {
        return instituicao;
    }

    public boolean isConheceProgramacao() {
        return conheceProgramacao;
    }

    public int getNivel() {
        return nivel;
    }

    public int getSaldoPontuacao() {
        return saldoPontuacao;
    }

    public void setId(int id) {
        this.id = id;
    }

    public char getGenero() {
        return genero;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void somaSaldo(int valor) {
        this.saldoPontuacao += valor;
    }

    @Override
    public String toString() {
        return "Aluno{" +
                "id=" + id +
                ", idUsuario=" + idUsuario +
                ", dtNascimento='" + dtNascimento + '\'' +
                ", genero=" + genero +
                ", apelido='" + apelido + '\'' +
                ", instituicao='" + instituicao + '\'' +
                ", conheceProgramacao=" + conheceProgramacao +
                ", nivel=" + nivel +
                ", saldoPontuacao=" + saldoPontuacao +
                '}';
    }
}
