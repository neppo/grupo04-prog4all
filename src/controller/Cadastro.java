package controller;

import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import DAO.AlunoDAO;
import model.Aluno;
import model.Usuario;

@WebServlet(name = "Cadastro", urlPatterns = {"/Cadastro"})
public class Cadastro extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType ("text/html;charset=utf-8");
        request.setCharacterEncoding("UTF-8");
        boolean conheceProgramacao = false;
        if (request.getParameter("opcao_prog").equals("Sim")) {
            conheceProgramacao = true;
        }

        Aluno aluno = new Aluno(request.getParameter("dtnasc-cadastro"), request.getParameter("opcao_sex").charAt(0), conheceProgramacao);
        Usuario usuario = new Usuario(request.getParameter("email-cadastro"), request.getParameter("senha-cadastro"));
        usuario.setPermissao(false);

        if (request.getParameter("inst-ensino") != null) {
            aluno.setInstituicao(request.getParameter("inst-ensino"));
        }
        if (request.getParameter("apelido-cadastro") != null) {
            aluno.setApelido(request.getParameter("apelido-cadastro"));
        }

        try {
            aluno = AlunoDAO.cadastroAluno(aluno, usuario, request.getParameter("nome-cadastro"));
        } catch (SQLException e) {
            aluno = null;
            e.printStackTrace();
        }

        if (aluno != null) {
            request.getSession().setAttribute("aluno", aluno);
            request.getSession().setMaxInactiveInterval(-1);
            response.sendRedirect("app.html");
        } else {
            request.setAttribute("erros-cadastro", "Email já cadastrado!");
            request.getRequestDispatcher("index.html").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
