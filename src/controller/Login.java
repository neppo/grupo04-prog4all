package controller;

import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import DAO.AlunoDAO;
import DAO.UsuarioDAO;
import model.Aluno;
import model.Usuario;

@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email-login");
        String senha = request.getParameter("senha-login");

        Usuario usuario = new Usuario(email, senha);
        Usuario usuarioCompleto = null;
        Aluno aluno = null;

        try {
            usuarioCompleto = UsuarioDAO.logaUsuario(usuario);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (usuarioCompleto.isPermissao()) {                                                // Significa que ele é administrador
            request.getSession().setAttribute("usuario", usuarioCompleto);
            request.getSession().setMaxInactiveInterval(-1);
            response.sendRedirect("app.html");
        } else if (usuarioCompleto.getId() == -1) {                                               // Significa que o login deu errado
            request.getRequestDispatcher("index.html").forward(request, response);
        } else {                                                                            // Significa que ele é aluno
            try {
                aluno = AlunoDAO.logaAluno(usuarioCompleto.getId());
            } catch (SQLException e) {
                e.printStackTrace();
            }
            request.getSession().setAttribute("aluno", aluno);
            request.getSession().setAttribute("usuario", usuarioCompleto);
            request.getSession().setMaxInactiveInterval(-1);
            response.sendRedirect("app.html");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
