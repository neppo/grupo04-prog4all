package DAO;

import model.Usuario;
import util.ConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioDAO {
    private static PreparedStatement pstm = null;
    private static ResultSet rs = null;
    private static Connection con;
    private static String sql = "";
    private static int qtdLinhasAfetadas = 0;

    public static Usuario logaUsuario(Usuario usuario) throws SQLException {
        con = ConnectionFactory.getConnection();

        sql = "SELECT * FROM USUARIO WHERE EMAIL = ? AND SENHA = ?";
        pstm = con.prepareStatement(sql);
        pstm.setString(1, usuario.getEmail());
        pstm.setString(2, usuario.getSenha());
        rs = pstm.executeQuery();

        usuario.setId(-1);

        while (rs.next()) {
            usuario.setPermissao(rs.getBoolean("permissao"));
            usuario.setId(rs.getInt("id"));
        }

        return usuario;
    }

    public static boolean verificaEmailCadastrado(Usuario usuario) throws SQLException {
        con = ConnectionFactory.getConnection();

        sql = "SELECT * FROM USUARIO WHERE EMAIL = ?";
        pstm = con.prepareStatement(sql);
        pstm.setString(1, usuario.getEmail());
        ResultSet idTabelaUsuario = pstm.executeQuery();

        if (!idTabelaUsuario.next()) {
            return true;
        } else {
            return false;
        }

    }

    public static int cadastroUsuario(Usuario usuario, String nome) throws SQLException {
        con = ConnectionFactory.getConnection();

        if (verificaEmailCadastrado(usuario)) {
            //Insere na tabela usuario
            sql = "INSERT INTO USUARIO (NOME, EMAIL, SENHA, PERMISSAO) VALUES (?,?,?,?)";

            // Prepara o comando com o preparestatment
            pstm = con.prepareStatement(sql);

            // Passa os parametros da consulta pra cada ? dentro da String sql

            pstm.setString(1, nome);
            pstm.setString(2, usuario.getEmail());
            pstm.setString(3, usuario.getSenha());
            pstm.setBoolean(4, false);

            // Executa o comando retornando no result a quantidade de linhas afetadas int
            qtdLinhasAfetadas = pstm.executeUpdate();

            sql = "SELECT * FROM USUARIO WHERE EMAIL = ?";
            pstm = con.prepareStatement(sql);
            pstm.setString(1, usuario.getEmail());
            ResultSet idTabelaUsuario = pstm.executeQuery();

            int idTabela = 0;
            while (idTabelaUsuario.next()) {
                idTabela = idTabelaUsuario.getInt("ID");
            }

            con.close();

            return idTabela;
        }

        return -1;
    }

}

class MainUsuario {
    public static void main(String[] args) {
        Usuario usuario = new Usuario("teste", "teste");
        try {
            UsuarioDAO.cadastroUsuario(usuario, "teste");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
