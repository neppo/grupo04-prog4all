package DAO;

import model.Aluno;
import model.PerguntasFrequentes;
import model.Usuario;
import util.ConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PerguntasFrequentesDAO {
    private static PreparedStatement pstm = null;
    private static ResultSet rs = null;
    private static Connection con;
    private static String sql = "";
    private static int qtdLinhaAfetadas = 0;

    public static ArrayList<PerguntasFrequentes> recuperaFAQ() throws SQLException {
        con = ConnectionFactory.getConnection();
        ArrayList<PerguntasFrequentes> FAQ = new ArrayList<>();
        PerguntasFrequentes FQAUnico = null;

        sql = "SELECT * FROM PERGUNTAS_FREQUENTES";
        pstm = con.prepareStatement(sql);
        rs = pstm.executeQuery();

        while (rs.next()) {
            FQAUnico = new PerguntasFrequentes(rs.getString("pergunta"),rs.getString("resposta"));
            FAQ.add(FQAUnico);
        }

        return FAQ;
    }
}

class MainFAQ {
    public static void main(String[] args) {

    }
}
