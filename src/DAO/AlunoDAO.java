package DAO;

import model.Aluno;
import model.Usuario;
import util.ConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AlunoDAO {
    private static PreparedStatement pstm = null;
    private static ResultSet rs = null;
    private static Connection con;
    private static String sql = "";
    private static int qtdLinhaAfetadas = 0;

    public static Aluno logaAluno(int idUsuario) throws SQLException {
        con = ConnectionFactory.getConnection();
        Aluno aluno = null;

        sql = "SELECT * FROM ALUNO WHERE ID_USUARIO = ?";
        pstm = con.prepareStatement(sql);
        pstm.setInt(1, idUsuario);
        rs = pstm.executeQuery();

        while (rs.next()) {
            aluno = new Aluno(rs.getString("nascimento"), rs.getString("genero").charAt(0), rs.getBoolean("conhecimento"));
            aluno.setIdUsuario(rs.getInt("id_usuario"));
            aluno.setId(rs.getInt("id"));
            aluno.setApelido(rs.getString("apelido"));
            aluno.setInstituicao(rs.getString("instituicao"));
            aluno.setNivel(rs.getInt("nivel"));
            aluno.setSaldoPontuacao(rs.getInt("saldo"));
        }

        return aluno;
    }

    public static Aluno cadastroAluno(Aluno aluno, Usuario usuario, String nome) throws SQLException {
        con = ConnectionFactory.getConnection();
        int idUsuario = UsuarioDAO.cadastroUsuario(usuario, nome);

        if (idUsuario != -1) {
            //Insere na tabela aluno
            sql = "INSERT INTO ALUNO (ID_USUARIO, GENERO, NASCIMENTO, APELIDO, INSTITUICAO, CONHECIMENTO) VALUES (?,?,?,?,?,?)";

            // Prepara o comando com o preparestat pstm =
            pstm = con.prepareStatement(sql);

            // Passa os parametros da consulta pra cada ? dentro da String sql

            String generoString = "" + aluno.getGenero();
            pstm.setInt(1, idUsuario);
            pstm.setString(2, generoString);
            pstm.setString(3, aluno.getDtNascimento());
            pstm.setString(4, aluno.getApelido());
            pstm.setString(5, aluno.getInstituicao());
            pstm.setBoolean(6, aluno.isConheceProgramacao());

            qtdLinhaAfetadas = pstm.executeUpdate();

            con.close();
        } else {
            aluno = null;
            System.out.println("Email já cadastrado!");
        }

        return aluno;
    }
}

class MainAluno {
    public static void main(String[] args) {
        Aluno aluno = new Aluno("10/03/1998", 'm', true);
        Usuario usuario = new Usuario("teste", "teste");

        try {
            AlunoDAO.cadastroAluno(aluno, usuario, "teste");
        } catch (SQLException e) {
            e.printStackTrace();
        }
//
//        try {
//            System.out.println(AlunoDAO.logaAluno(1));
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
    }
}
