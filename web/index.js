function login(login, senha) {
    $.ajax({
        url: "Login",
        type: 'POST',
        data: {
            emailLogin: login,
            senhaLogin: senha
        },
        beforeSend: function () {
        }
    })
        .done(function () {
            if (resultado === null) {
                console.log("Email ou senha incorreta!");
            }
        })
        .fail(function (jqXHR, textStatus, resultado) {
            if (jqXHR["status"] === 500) {
                console.log("Erro 500, não foi possível estabelecer conexão com o servidor!");
            } else if (jqXHR["status"] === 502) {
                console.log("Erro 502, não foi possível estabelecer conexão!");
            } else if (jqXHR["status"] === 404) {
                console.log("Erro 404, não foi encontrado o diretório solicitado!");
            }
        });
}
